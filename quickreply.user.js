// ==UserScript==
// @name         Isleward - reply to whisper with /r
// @namespace    Isleward.Addon
// @version      0.1
// @description  Allows to reply to last whisper with /r
// @author       Qndel
// @match        play.isleward.com*
// @grant        none
// ==/UserScript==
function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
(function () {
    var fun = function(){
        if(jQuery(".el.textbox.message")[0] != undefined && jQuery(".el.textbox.message").val().substring(0, 2) == "/r" && window.lastReply != undefined){
                            jQuery(".el.textbox.message").val(jQuery(".el.textbox.message").val().replace("/r", "@"+window.lastReply+" "));
                        }
    }
    setInterval(fun,100);
    addons.register({
        init: function(events) {
            events.on('onGetMessages', this.onGetMessages.bind(this));
        },
        onGetMessages: function(msg) {
            if(msg.messages && msg.messages[0] != undefined && msg.messages[0].type != undefined && msg.messages[0].type == "chat" && msg.messages[0].message != undefined){
                var myReg =   /\((\b.*) to you\): \b.*/g;
                var matched = myReg.exec(msg.messages[0].message);
                if(matched != undefined && matched.length == 2){
                    window.lastReply = matched[1];
                }
            }
        }
    });
}));